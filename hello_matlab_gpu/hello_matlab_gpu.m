%% Verifying Hardware
% To run this demo, you need to have a supported GPU card. See the
% documentation for more information. The command |gpuDevice| or
% |gpuDeviceCount| can be used to see if your machine has a supported GPU
% card.

try
   g = gpuDevice;
   GPU_TYPE = g.Name
catch ME
   error(ME.identifier, ME.message);
end


