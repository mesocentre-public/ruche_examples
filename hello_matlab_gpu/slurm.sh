#!/bin/bash

#SBATCH --job-name=job-matlab-gpu
#SBATCH --output=%x.o%j 
#SBATCH --time=00:10:00 
#SBATCH --ntasks=1
#SBATCH --partition=gpu_test
#SBATCH --gres=gpu:1


# Module load
module purge
module load matlab/R2022a/intel-20.0.4.304

matlab -nodisplay -r hello_matlab_gpu
