# Sample python job

Configure the python environment using OpenMPI.

```shell
module load anaconda3/2020.02/gcc-9.2.0
module load openmpi/4.1.1/gcc-11.2.0
module load gcc/11.2.0/gcc-4.8.5 
conda create -n mpi4py-env
source activate mpi4py-env
export MPICC=$(which mpicc)
conda install pip
pip install mpi4py --no-cache-dir
```

Submit the job to the scheduler.

```shell
sbatch slurm.sh
```

*Note that a naive installation using*
```
conda install mpi4py
```
*is possible but will be less efficient.*
