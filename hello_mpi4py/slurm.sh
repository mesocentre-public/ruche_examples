#!/bin/bash

#SBATCH --job-name=hello_mpi4py
#SBATCH --output=%x.o%j 
#SBATCH --time=00:20:00 
#SBATCH --nodes=2 
#SBATCH --ntasks=40 
#SBATCH --ntasks-per-node=20 
#SBATCH --partition=cpu_short

# Load necessary modules
module purge
module load anaconda3/2020.02/gcc-9.2.0
module load openmpi/4.1.1/gcc-11.2.0
module load gcc/11.2.0/gcc-4.8.5

# Activate anaconda environment
source activate mpi4py-env

# Run python script
srun python hello_mpi4py.py
