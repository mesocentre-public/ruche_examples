module load anaconda3/2022.10/gcc-11.2.0
module load cuda/12.2.1/gcc-11.2.0
conda create --name jax
source activate jax
conda install pip
pip install -U "jax[cuda12]"
conda env export > config/environment_jax.yml # save conda environment description
