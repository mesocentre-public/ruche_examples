from jax.lib import xla_bridge
print(xla_bridge.get_backend().platform)

import jax.numpy as jnp
def selu(x, alpha=1.67, lmbda=1.05):
  return lmbda * jnp.where(x > 0, x, alpha * jnp.exp(x) - alpha)

x = jnp.arange(5.0)
print(selu(x))
