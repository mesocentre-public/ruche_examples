#!/bin/bash
#SBATCH --job-name=jax_test
#SBATCH --output=%x.o%j
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu_test

# Module load
module load anaconda3/2022.10/gcc-11.2.0
module load cuda/12.2.1/gcc-11.2.0

# Activate anaconda environment code
source activate jax

# Train the network
time python scripts/test.py

