module load anaconda3/2022.10/gcc-11.2.0
module load cuda/11.8.0/gcc-11.2.0
conda create --name pytorch
source activate pytorch
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
conda env export > config/environment_pytorch.yml # save conda environment description
