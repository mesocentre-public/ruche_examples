#!/bin/bash
#SBATCH --job-name=pytorch_train
#SBATCH --output=%x.o%j
#SBATCH --time=01:00:00
#SBATCH --ntasks=4
#SBATCH --partition=cpu_short
#SBATCH --export=NONE

# Module load
module load anaconda3/2022.10/gcc-11.2.0
module load cuda/11.8.0/gcc-11.2.0

[ ! -d output ] && mkdir output

# Activate anaconda environment code
source activate pytorch

# Train the network
time python scripts/train_network.py 
